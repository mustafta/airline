/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Airline;

import java.util.Date;

/**
 *
 * @author Taghreed
 */
public class Flight {
    private String flightNumber;
    private Date date;
    
    public Flight(){}
    
    public Flight (String flightNumber, Date date)
    {
    this.flightNumber=flightNumber;
    this.date=date;
    
    }
    public String getFlightNumber()
    {
    
    return this.flightNumber;
    }
    
    public Date getDate(){
    return this.date;
    }
    
}
