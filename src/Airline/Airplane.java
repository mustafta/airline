/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Airline;



/**
 *
 * @author Taghreed
 */
public class Airplane {
    private String make;
    private String model;
    private int id;
    private int seatNumber;
    
    public Airplane (){}
    public Airplane (String make, String model, int id,int seatNumber)
    {
    this.make=make;
    this.model=model;
    this.id=id;
    this.seatNumber=seatNumber;
    }
    
    public String getMake()
    {
    return this.make;
    }
    
    public String getModel()
    {
    return this.model;
    }
    
    public int getId(){
    return this.id;
    }
    
    public int getSeatNumber(){
    return this.seatNumber;
    }
}
    

