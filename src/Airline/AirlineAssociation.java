/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Airline;

/**
 *
 * @author Taghreed
 */
public class AirlineAssociation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Flight flight1= new Flight();
       Airplane airplane = new Airplane("USA", "Boeing 737-800", 234, 12 );
       Airport airport = new Airport("ABC123","Baghdad");
       Airlinet airline1 = new Airlinet ("Canada airline", "AD23");
       
       System.out.println("Flight Number: "+ flight1.getFlightNumber()+ " \t " +"Flight Date: " + flight1.getDate());
       System.out.println("AirPlane Make: "+ airplane.getMake()+ " \t "+ "AirPlane Model: " + airplane.getModel()+ " \t "+ "AirPlane ID: "+ airplane.getId()+ " \t " + "AirPlane Seat Number: "+ airplane.getSeatNumber());
       System.out.println("AirPort Code: "+ airport.getCode()+ " \t " + "AirPort City: " + airport.getCity());
        System.out.println("AirLine Name: "+ airline1.getAirlineName()+ " \t " + "Airline Code: " + airline1.getCode());
    }
    
}
