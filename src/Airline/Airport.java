/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Airline;

/**
 *
 * @author Taghreed
 */
public class Airport {
    private String code;
    private String city;
   
    
    public Airport(){}
    public Airport (String code, String city)
    {
    this.code=code;
    this.city=city;
    }
    
    public String getCode()
    {
    return this.code;
    }
    
    public String getCity()
    {
    return this.city;
    }
}
